antiSMASH skeleton to assist plugin development
===============================================

This script is designed to help writing antiSMASH plugins without having to have
the full antiSMASH script up and running.

**Note:** So far, only cluster detection plugins are supported.

Working with the skeleton
-------------------------


License
-------
This code is under the same license as antiSMASH itself, the GNU AGPL version 3.
See the [`LICENSE.txt`](LICENSE.txt) file for details.
